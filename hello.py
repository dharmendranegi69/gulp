"""
    This lambda lists the participant details in the participant dashboard for the organiser
    It takes event id (for example, "container-conf") and host_id (for example, "1528205024")
    as inputs and returns a JSON that provides the list of participants,
    event name and validity as the return value;
    before returning, the function must check if the user is the event host.
"""
import json
import logging
import os
from datetime import datetime
from pprint import pprint

import boto3

from botocore.errorfactory import ClientError
from Utils.data_layer import DBConnection
from Utils.s3operations import S3Operations

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

DB_CONNECTION = DBConnection()

USER_BUCKET_NAME = os.environ['USER_BUCKET_NAME']

user_s3_obj = S3Operations(USER_BUCKET_NAME)

CONFERENCE_BUCKET_NAME = os.environ['CONFERENCE_BUCKET_NAME']
conference_s3_obj = S3Operations(CONFERENCE_BUCKET_NAME)

client = boto3.client('s3')


def get_form_details(hosted_event_id):
    """
       getting form details from the s3 bucket i.e number of form available
    """
    try:
        response = client.get_object(Bucket=CONFERENCE_BUCKET_NAME,
                                     Key=f"hosted-events/{hosted_event_id}/form_data.json")
        form_details = json.loads(response["Body"].read().decode())
        # print('form',form_details)
        list_of_form_name = [form['name'] for form in form_details['forms']]
        item_list = [name for name in list_of_form_name if
                     name not in ('name', 'designation', 'organisation', 'email_id', 'phone_number')]
        return_value = item_list, form_details['forms']
    except ClientError as error:
        LOGGER.error(error)
        return_value = 400
    return return_value


def get_participant_details_json(hosted_event_id):
    """
    :param hosted_event_id: the event id (e.g., "container-conf") for returning participant details
    :return: json of the participants list
    """
    command = f"SELECT email_id, discount_code, designation, name, payment_id, organisation," \
              f"ticket_type, time_stamp, ticket_status, event_ticketing.booking_id, misc, event_ticketing.event_name, " \
              f"ticket_price, phone_number, paid_amount, utm_source, utm_medium, utm_campaign, checked_in, check_in_time " \
              f"FROM public.event_ticketing LEFT JOIN participant_table ON participant_table.booking_id = event_ticketing.booking_id WHERE " \
              f"event_ticketing.event_name = '{hosted_event_id}' AND " \
              f"ticket_status != 'LEAD' AND ticket_status NOT LIKE '%WAITLIST%' ORDER BY time_stamp DESC"

    rows = DB_CONNECTION.execute_sql_query(command, 'event_display_participant_details')
    form_details = get_form_details(hosted_event_id)
    tickets = []
    for row in rows:
        utm_source = str(row[15]) if row[15] is not None else '-'
        utm_medium = str(row[16]) if row[16] is not None else '-'
        utm_campaign = str(row[17]) if row[17] is not None else '-'
        checked_in = str(row[18]) if row[18] is not None else '-'
        check_in_time = str(row[19]) if row[19] is not None else '-'

        ticket_entry = {
            "email_id": str(row[0]),
            "discount_code": str(row[1]),
            "designation": str(row[2]),
            "name": str(row[3]),
            "payment_id": str(row[4]),
            "organisation": str(row[5]),
            "ticket_type": str(row[6]),
            "time_stamp": str(row[7]),
            "ticket_status": str(row[8]),
            "booking_id": str(row[9]),
            "event_name": str(row[11]),
            "ticket_price": str(row[12]),
            "phone_number": str(row[13]),
            "paid_amount": str(row[14]),
            "utm_source": utm_source,
            "utm_medium": utm_medium,
            "utm_campaign": utm_campaign,
            "checked_in": checked_in,
            "check_in_time": check_in_time
        }
        # converting the data is miscellaneous column to individual columns
        # using ast.literal_eval to convert string to dict

        # in case of WAITLISTING, we are getting NoneType for row[10]

        if row[10] is None or not row[10].strip():
            misc = ''
        else:
            try:
                update_dic = json.loads(row[10])
                update_dic = {x.replace(' ', '_'): v for x, v in update_dic.items()}
                print(".....", update_dic)
                form_list = form_details[0]
                for form in form_list:
                    underscore_form = (form.replace(' ', '_')).lower()
                    if underscore_form not in update_dic.keys():
                        update_dic[underscore_form] = "NA"
                misc = {'misc': update_dic}
            except:
                misc = ''
        ticket_entry.update(misc)
        tickets.append(ticket_entry)
    return tickets, form_details


def validate_if_user_is_the_event_host(hosted_event_id, user_id):
    """
    Checks if the event host_is is the host of the hosted event or not
    :param hosted_event_id: event id given by the front-end
    :param user_id: user who claims to be the host of the event
    :return: boolean value
    """
    file_name = f"{user_id}.json"
    s3_response = user_s3_obj.get_data_from_s3(file_name)
    # s3_response will be 0 if there was an error fetching the details from S3
    if s3_response:
        user_data = s3_response
        return_value = hosted_event_id in user_data["hosted_event_ids"]
    else:
        return_value = 0
    return return_value


def check_event_validity(event_end_date):
    """
        Function to check the date validity of the event
    :param event_end_date: the end date of the event
    :return: 1 if the event is a past event else returns 0
    """

    end_date = datetime.strptime(event_end_date, '%Y-%m-%d')
    todays_date = datetime.now().date()

    is_past_event = bool(todays_date > end_date.date())
    return is_past_event


def get_event_details_from_s3(hosted_event_id, file_name):
    """
        Gets the event details of the conference from S3 bucket for the given file name
        :param hosted_event_id: hosted_event_id of the event and file name
        :return: organiser_details information and event details as based on file name of the json
        """
    file_name = f"hosted-events/{hosted_event_id}/{file_name}.json"
    s3_response = conference_s3_obj.get_data_from_s3(file_name)

    # s3_response will be 0 if there was an error fetching the details from S3
    if s3_response:
        event_details = s3_response
    else:
        event_details = ''
    return event_details


def lambda_handler(event, context):
    """
    :param event: a JSON with following example format:
        {"hosted_event_id" : "container-conf", "user_id" : "1528205024"}
    :param context: the lambda context object
    :return: the JSON with participant details
    """
    LOGGER.info('input to lambda: %s', event)
    hosted_event_id = event["hosted_event_id"]
    user_id = event["user_id"]

    is_user_the_event_host = validate_if_user_is_the_event_host(
        hosted_event_id, user_id)

    # get the participant details, event name and the event's validity for the given event id
    if is_user_the_event_host:
        organiser_details = get_event_details_from_s3(hosted_event_id, 'organiser_details')
        organiser_mail_id = ''
        if organiser_details:
            organiser_mail_id = organiser_details['organiser_mail']

        participant_details, form_data = get_participant_details_json(hosted_event_id)
        event_details = get_event_details_from_s3(hosted_event_id, 'event_details')
        event_name = event_details["event_name"]
        event_end_date = event_details["event_end_date"]
        is_past_event = check_event_validity(event_end_date)

        return_value = {
            'event_name': event_name,
            'is_past_event': is_past_event,
            'participant_details': participant_details,
            'organiser_mail': organiser_mail_id,
            'form_details': form_data
        }
    else:
        LOGGER.info('User not authenticated to load the participant details')
        return_value = 403  # forbidden
    return return_value


if __name__ == "__main__":
    hosted_event = {
        "user_id": '1551250284',
        "hosted_event_id": "testkonfhube5f9bb0f"
    }
    pprint(lambda_handler(hosted_event, ""))
