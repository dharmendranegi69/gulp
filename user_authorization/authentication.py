"""
    Lambda to load a user profile
"""
import json
import logging
import os

import boto3
import firebase_admin
from firebase_admin import auth

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

client = boto3.client('s3')
BUCKET_NAME = 'konfhub-user-details'

default_app = firebase_admin.initialize_app()


def authorize_user(headers):
    """
        Returns "Allow"/"Deny" according to authorization logic
    """
    try:
        user_id = headers["user_id"]
        file_name = f'{user_id}.json'
        response = client.get_object(Bucket=BUCKET_NAME, Key=file_name)
        user_data = json.loads(response['Body'].read().decode())
        email_id_from_konfhub = user_data['emailId']

        firebase_id = headers["firebase_id"]
        email_id_from_firebase = get_email_from_firebase(firebase_id)
        print(email_id_from_firebase)
        print(email_id_from_konfhub)
        print(headers["event_id"])
        print(user_data['hosted_event_ids'].split(','))
        effect = "Allow" if (email_id_from_firebase.strip() in email_id_from_konfhub.strip()) and (
                headers["event_id"] in user_data['hosted_event_ids'].split(', ')) else "Deny"
    except Exception as exception:
        print(exception)
        effect = "Deny"
    return effect


def get_email_from_firebase(firebase_id):
    """
        This returns email ID using the firebase ID
    """
    try:
        user = auth.get_user(firebase_id)
        email_id_returned_by_firebase = user.email
        return email_id_returned_by_firebase
    except Exception as ex:
        print(ex)
        return "Not authorized"


def lambda_handler(event, context):
    """
        Lambda Handler to authorizer the user
    """
    LOGGER.info("input to lambda {}".format(event))
    headers = event["headers"]
    effect = authorize_user(headers)

    response_body = {
        "principalId": "user",
        "policyDocument": {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": "execute-api:Invoke",
                    "Effect": effect,
                    "Resource": event["methodArn"]
                }
            ]
        }
    }
    return response_body


if __name__ == '__main__':
    event = {

        "user_id": "1528350932",
        "event_id": "testkonfhubeventidtest6fa906ea",
        "firebase_id": "23pfXxc5KJRLNmFg1x7vPA6fVrI3",
        "methodArn" : ""
    }
    os.environ['bucket_name'] = "konfhub-user-details"
    print(lambda_handler(event, " "))
    del os.environ['bucket_name']
